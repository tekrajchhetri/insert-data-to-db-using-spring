package com.tekrajchhetri.blog.springdbinsert.repository;

import com.tekrajchhetri.blog.springdbinsert.entity.Project;
import org.springframework.data.repository.CrudRepository;

public interface ProjectRepository extends CrudRepository<Project, Long> {
}
