package com.tekrajchhetri.blog.springdbinsert;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringdbinsertApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringdbinsertApplication.class, args);
	}

}
