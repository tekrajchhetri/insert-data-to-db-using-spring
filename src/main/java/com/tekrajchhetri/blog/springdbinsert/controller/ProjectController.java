package com.tekrajchhetri.blog.springdbinsert.controller;

import com.tekrajchhetri.blog.springdbinsert.entity.Project;
import com.tekrajchhetri.blog.springdbinsert.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;


@Controller
public class ProjectController {

    @Autowired
    ProjectRepository projectRepository;

    @GetMapping("/")
    public String displayForm(Model model){
        model.addAttribute("formdata", new Project());
        return "index";
    }

    @PostMapping("/save")
    public String Save(Model model, Project project){
        projectRepository.save(project);
        return "redirect:/";
    }
}
