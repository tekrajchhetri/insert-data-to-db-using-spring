# Inserting form data into database using Spring
------
The source code here is a part of blog post describing how can you insert data into database using spring framework.

## Tools / Required Spring dependency
* Spring Web
* Thymeleaf
* H2 for inmemory database
* JPA (Hibernate)

## Running
* localhost:8082
* DB:http://localhost:8082/h2-console/
* JDBC URL for DB: jdbc:h2:mem:testdb if it is different than this, change to it.